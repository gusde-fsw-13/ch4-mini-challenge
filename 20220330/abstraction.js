{
  class Human {
    constructor(props){
      if(this.constructor === Human){
        throw new Error("Cannot instantiate from Abstract Class");
      }
      let {name, address} = props;
      this.name = name;
      this.address = address;
      this.profession = this.constructor.name;
    }

    work(){
      console.log("Working...");
    }

    introduce(){
      console.log(`Hello, my name is ${name}`);
    }
  }

  class Police extends Human {
    constructor(propos){
      super(propos);
      this.rank = propos.rank;
    }

    work(){
      console.log("Go to the police station");
      super.work();
    }
  }

  const wiranto = new Police({
    name: "Wiranto",
    address: "Unknown",
    ranl: "General"
  });

  console.log(wiranto.profession);
}